import numpy as np
from time import time, sleep
from collections import deque

from settings import s

import logging
import random


# tensorflow imports
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
from collections import deque


class NeuralNetwork:
    def __init__(self, state_size, action_size):
        '''
        Initializing the Neural network with the current boardstate

        self.state will be filled with the following information:
        arena , self, others, coins,  bombs + timer  , explosions
        -1/0/1,  2  ,   3   ,   4  ,    5  + timer(4),  8 + timer(2)

        '''

        ############### logging for debugging ###############
        self.name = 'Skynet'
        self.logger = logging.getLogger(self.name + '_logger')
        self.logger.setLevel(logging.DEBUG)

        log_dir = f'agent_code/my_agent/logs/'
        handler = logging.FileHandler(f'{log_dir}{self.name}.log', mode='w')
        handler.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s: %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

        self.logger.info(f'Activated NeuralNetwork')
        ############### logging for debugging ###############


        ################ tensorflow attempt ################

        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.01
        self.learning_rate = 0.001
        self.model = self._build_model()
        ################ tensorflow attempt ################

    def __bool__(self):
        return bool(self.state_size or self.action_size)
    #--------------------------------------------------------------------

    def _build_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma * np.amax(self.model.predict(next_state)[0]))
                target_f = self.model.predict(state)
                target_f[0][action] = target
                self.model.fit(state, target_f, epochs=1, verbose=0)
            if self.epsilon > self.epsilon_min:
                self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)

    def get_reward(self, type):
        if type == "crate":
            self.reward += 5
        elif type == "coin":
            self.reward += 20
        elif type == "kill":
            self.reward += 100
        elif type == "dead":
            self.reward -= 200



    #--------------------------------------------------------------------

    def predict(self, state):
        '''
        Return Probabaility Values that represent the prediction
        '''
        self.logger.info('Skynet trying to Predict actions')
        prediction = (self.act(state))
        output = ['UP', 'DOWN' , 'RIGHT', 'LEFT', 'BOMB', 'WAIT']
        self.logger.info('Skynet chose ' + output[prediction])
        return output[prediction]

def generate_state(self, x, y, arena, bombs_left, bombs, others, coins, explosions):
    self.state = []

    # fill arena data with more nesseccarry information needed for the NN
    # player coordinates
    arena[x,y] = 2
    for coin in coins:
        arena[coin[0], coin[1]] = 4
    for (x,y) in others:
        arena[x,y] = 3
    for bomb in bombs:
        arena[bomb[0], bomb[1]] = 9 - bomb[2]

    # append data of arena and explosions into state
    arena = arena.flatten()
    explosions = explosions.flatten()
    for element in arena:
        self.state.append(element)
    for element in explosions:
        self.state.append(element)
    self.state.append(bombs_left)
    return self.state

def act(self):
    self.logger.info('Entering "Act-function" ')

    arena = self.game_state['arena']
    x, y, _ , bombs_left, score = self.game_state['self']
    bombs = self.game_state['bombs']
    others = [(x,y) for (x,y,n,b,s) in self.game_state['others']]
    coins = self.game_state['coins']
    explosions = self.game_state['explosions']


    bomb_map = np.ones(arena.shape) * 5
    for xb , yb, t in bombs:
        for (i,j) in [(xb+h, yb) for h in range(-3,4)] + [(xb,yb+h) for h in range(-3,4)]:
            if (0 < i < bomb_map.shape[0]) and (0 < j < bomb_map.shape[1]):
                bomb_map[i,j] = min(bomb_map[i,j], t)

    if self.coordinate_history.count((x,y)) > 2:
        self.ignore_others_timer = 5
    else:
        self.ignore_others_timer -= 1
    self.coordinate_history.append((x,y))
    self.logger.info('Starting NeuralNetwork')
    generate_state(self, x, y, arena, bombs_left, bombs, others, coins, explosions)
    self.next_action = self.skynet.predict(self.state)





def setup(self):
    '''
    Called once before a set of games to initialize data structures

    the "self" object passed to this method will be the same in all other callback methods.
    You can assign new properties.
    '''
    self.logger.debug('Successfully entered setup code')
    np.random.seed()
    self.bomb_history = deque ( [], 5 )
    self.coordinate_history = deque( [], 20 )
    self.ignore_others_timer = 0


    self.skynet = NeuralNetwork( 579 , 6 )

def reward_update(self):
    '''
    Rewarding NN
    '''
    self.logger.debug(f'Encountered {len(self.events)} game event(s)')

def end_of_episode(self):
    '''
    Hand out final rewards and do training.

    and log encountered gamestates
    '''

    self.logger.debug(f'Encountered {len(self.events)}) game events(s) in final step')
